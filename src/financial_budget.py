from bs4 import BeautifulSoup
from urllib.request import urlopen
import ssl
import os

class FinancialBudget:
    def __init__(self, url):
        context = ssl._create_unverified_context()
        self.html = urlopen(url, context=context)
        self.soup = BeautifulSoup(self.html, 'html.parser')

    def find_last_year(self, search_ele_list):
        element_text = ''
        tr_tag = self.soup.find_all('tr')
        for tr in tr_tag:
            try:
                td_tag = tr.find_all('td')
                for td in td_tag:
                    match_ind = -1
                    for ele in search_ele_list:
                        if match_ind > -1:
                            break
                        if td.text.strip().find(ele) == 0:
                            last_ind = len(td_tag) - 1
                            for x in range(last_ind, -1, -1):
                                element_text = td_tag[x].text.strip()
                                if element_text:
                                    match_ind = x
                                    break
                    for ele in search_ele_list:
                        if td.text.strip().find(ele) == 0:
                            element_text = td_tag[match_ind].text.strip()
                            yield element_text
            except IndexError:
                element_text = ''
                print('index error')
                yield element_text