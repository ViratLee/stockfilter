from flask import Flask
from flask import jsonify
import sys
import os
import randomsymbol as rm
from random import randint
import json
import company_detail as company
import financial_budget as fnbd
from matching_company import MatchingCompany

app = Flask(__name__)

@app.route("/")
def home():
    return "matching company."

@app.route("/random")
def random_no_filter():
    assets = ''
    roe = ''
    margin = ''
    p_bv = ''
    dvd_yield = ''
    url = ''
    symbol_detail = None
    with open('listedCompanies.json', encoding='utf8') as json_file:
        data = json.load(json_file)
        random_symbol = rm.RandomSymbol(data)
        symbol_detail = random_symbol.random_symbol()
        matchingCompany = MatchingCompany()
        result, url = matchingCompany.get_detail(symbol_detail = symbol_detail)
    try:
        if result is not None:
            assets = next(result)
            roe = next(result)
            margin = next(result)
            p_bv = next(result)
            dvd_yield = next(result)
    except:
        print("Unexpected error:", sys.exc_info()[0])
    detail = 'symbol:{}, assets:{}, roe:{}, margin:{}, p_bv:{}, dvd_yield:{}'.format(symbol_detail['symbol'],assets,roe,margin,p_bv,dvd_yield)
    print(detail)
    #return render_json(symbol_detail, assets, roe, margin, p_bv, dvd_yield,url)
    return render_html(symbol_detail, assets, roe, margin, p_bv, dvd_yield,url)

@app.route('/pbv/<val>')
def pvb_over(val):
    com, symbol_detail = find_pbv_under(val)
    return render_html(symbol_detail, com['assets'], com['roe'], com['margin'], com['p_bv'], com['dvd_yield'],com['url'])

@app.route('/roe/<val>')
def roe_over(val):
    com, symbol_detail = find_roe_over(val)
    return render_html(symbol_detail, com['assets'], com['roe'], com['margin'], com['p_bv'], com['dvd_yield'],com['url'])
    
def find_roe_over( val = None, random_limit = 5):
    com = None
    symbol_detail = None
    if val is not None:
        with open('listedCompanies.json', encoding='utf8') as json_file:
            data = json.load(json_file)
            count = len(data['all'])
            random_count = 0
            while com is None:
                random_count += 1
                random_number = randint(0, count - 1)
                random_symbol = rm.RandomSymbol(data)
                symbol_detail = random_symbol.get_symbol_detail(random_number)
                matchingCompany = MatchingCompany()
                com = matchingCompany.find_match_roe(roe_over = val, symbol_detail = symbol_detail)
                if random_count >= random_limit:
                    print('random over limit:{}, stop search.'.format(random_count))
                    break
            #detail = 'match with symbol:{} roe:{}, margin:{}'.format(symbol_detail['symbol'],com['roe'],com['margin'])
            #print()
    else:
        print('find_roe_over, val None')
        raise KeyError('no roe filter value')
    return com, symbol_detail

def find_pbv_under( val = None, random_limit = 5):
    symbol_detail = None
    com = None
    if val is not None:
        with open('listedCompanies.json', encoding='utf8') as json_file:
            data = json.load(json_file)
            count = len(data['all'])
            random_count = 0
            while com is None:
                random_count += 1
                random_number = randint(0, count - 1)
                random_symbol = rm.RandomSymbol(data)
                symbol_detail = random_symbol.get_symbol_detail(random_number)
                matchingCompany = MatchingCompany()
                com = matchingCompany.find_match_pbv(pbv_under = val, symbol_detail = symbol_detail)
                if random_count >= random_limit:
                    print('random over limit:{}, stop search.'.format(random_count))
                    break
            print('match with symbol:{} P/BV:{}'.format(symbol_detail['symbol'] ,com['p_bv']))
    else:
        print('find_pbv_under, val None')
        raise KeyError('no P/BV filter value')
    return com, symbol_detail
    
def render_json(symbol_detail, assets,roe,margin,p_bv,dvd_yield, url):
    symbol = symbol_detail['symbol']
    link_url = '<a href=\"'+url+'\">'+symbol+'</a>'
    desc = symbol_detail['description']
    text_json = '{"desc": "'+desc+'","assets":"'+assets+'","roe":"'+roe+'", "margin": "'+margin+'","P/BV":"'\
    +p_bv+'","dvd_yield":"'+dvd_yield+'","url":"'+link_url+'"}'
    jd = json.loads(text_json)
    json_str = json.dumps(jd, ensure_ascii=False, indent=4)
    return json_str
    # print(json_str)
    # json_dict = json.loads(str.encode(json_str,"utf-8"))
    # return jsonify(json_dict)

def render_html(symbol_detail, assets,roe,margin,p_bv,dvd_yield, url):
    url = '<a href=\"{}\">{}</a>'.format(url,symbol_detail['symbol'])
    return '''
       Symbol:{} <br>
       ROE(%):{} <br>
       อัตรากำไรสุทธิ(%):{} <br>
       P/BV (เท่า):{} <br>
       อัตราส่วนเงินปันผลตอบแทน(%){} <br>
       ดูรายละเอียด:{} 
    '''.format(symbol_detail['symbol'], roe, margin, p_bv, dvd_yield, url)

# if __name__ == '__main__':
#     app.run()