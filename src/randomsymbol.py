from random import randint
class RandomSymbol:
    def __init__(self, data):
        self.data = data

    def random_symbol(self):
        count = len(self.data['all'])
        random_number = randint(0, count - 1)
        return self.data['all'][random_number]
    
    def get_symbol_detail(self, random_no):
        return self.data['all'][random_no]