from bs4 import BeautifulSoup
from urllib.request import urlopen
import ssl
import os

class CompanyDetail:
    def __init__(self, url):
        context = ssl._create_unverified_context()
        self.html = urlopen(url, context=context)
        self.soup = BeautifulSoup(self.html, 'html.parser')

    def last_price(self):
        last_price = ''
        found = False
        count = 0
        for data in self.soup.find_all('table', {'class': 'table'}):
            if count > 0:
                break
            tr_tag = data.find_all('tr')
            for tr in tr_tag:
                if count > 0:
                    break
                td_tag = tr.find_all('td')
                if count > 0:
                    break
                for td in td_tag:
                    try:
                        if td.text.strip().find("ล่าสุด") == 0:
                            found = True
                            count = 1
                            tag_td = td.find_next_sibling("td")
                            print('---ราคาล่าสุด {}---'.format(tag_td.text))
                    except IndexError:
                        last_price = '-----ไม่เจอราคา----------'
        return last_price

    def last_roe(self):
        roe = ''
        count = 0
        for table_tag in self.soup.find_all('table', {'class': 'table'}):
            tr_tag = table_tag.find_all('tr')
            for tr in tr_tag:
                td_tag = tr.find_all('td')
                for td in td_tag:
                    if td.text.strip().find("ROE(%)") == 0:
                        roe = td_tag[-1].text.strip()
                        print('ROE(%):{}'.format(td_tag[-1].text.strip()))
        return roe
